<img src='/images/gwenbell.jpg', width='100%', class='profile' />

### GWEN BELL 

+ [GAMAN](http://gwenbell.com/gaman)
+ [Align Your Website](http://align.gwenbell.com)
+ [Git Commit](http://git.gwenbell.com)

<div id="mc_embed_signup"><form action="http://gwenbell.us7.list-manage.com/subscribe/post?u=9a3b9ea24469d6d86a5bd1626&amp;id=354f3e7685" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate><label for="mce-EMAIL">GET FREE UPDATES</label><input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="you@youremail.com" required><div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div></form></div>

***

[Contact](mailto:gwen@gwenbell.com) | [About](/about) | [Bugs](http://gitlab.com/u/gwenbell)

