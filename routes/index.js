/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'Gwen Bell' });
};

exports.whyxnoty = function(req, res){
  res.render('whyxnoty', { title: 'Gwen Bell | Why x Not y?' });
};

exports.appreciations = function(req, res){
  res.render('appreciations', { title: 'Gwen Bell | Appreciations' });
};

exports.tmux = function(req, res){
  res.render('tmux', { title: 'Gwen Bell | Tmux' });
};

exports.gaman = function(req, res){
  res.render('gaman', { title: 'Gwen Bell | Gaman' });
};

exports.gnome = function(req, res){
  res.render('gnome', { title: 'Gwen Bell | What Begins with G?' });
};

exports.irc = function(req, res){
  res.render('irc', { title: 'Gwen Bell | IRC' });
};

exports.ask = function(req, res){
  res.render('ask', { title: 'Gwen Bell | Ask' });
};

exports.expat = function(req, res){
  res.render('expat', { title: 'Gwen Bell | Expat' });
};

exports.dt = function(req, res){
  res.render('dt', { title: 'Gwen Bell | Dominic Tarr' });
};

exports.path = function(req, res){
  res.render('path', { title: 'Gwen Bell | Path' });
};

exports.write = function(req, res){
  res.render('write', { title: 'Gwen Bell | Write' });
};

exports.like = function(req, res){
  res.render('like', { title: 'Gwen Bell | Like' });
};

exports.tech = function(req, res){
  res.render('tech', { title: 'Gwen Bell | Tech' });
};

exports.book = function(req, res){
  res.render('book', { title: 'Gwen Bell | Book' });
};

exports.about = function(req, res){
  res.render('about', { title: 'Gwen Bell | About' });
};

exports.moretruth = function(req, res){
  res.render('moretruth', { title: 'Gwen Bell | More Truth' });
};

exports.whatitslike = function(req, res){
  res.render('whatitslike', { title: 'Gwen Bell | What It\'s Like' });
};

exports.arch = function(req, res){
  res.render('arch', { title: 'Gwen Bell | Arch' });
};

exports.pairprogram = function(req, res){
  res.render('pairprogram', { title: 'Gwen Bell | Pair Programming' });
};

exports.gitnotgithub = function(req, res){
  res.render('gitnotgithub', { title: 'Gwen Bell | Git Not GitHub' });
};

exports.tentools = function(req, res){
  res.render('tentools', {title: 'Gwen Bell | Ten Tools' });
};

exports.whybitters = function(req, res){
  res.render('whybitters', {title: 'Gwen Bell | Why Bitters'});
};

exports.osex = function(req, res){
  res.render('osex', {title: 'Gwen Bell | OSex'});
};

exports.revolution = function(req, res){
  res.render('revolution', {title: 'Gwen Bell | Ghost'});
};


