### TECHNICAL WRITING

> The Linux philosophy is 'Laugh in the face of danger'. Oops. Wrong one. 'Do it yourself'. Yes, that's it. - Linus Torvalds

#### Current Tech Pieces

1. [Why x Tech, Not y Tech?](/whyxnoty)
1. [tmux](/tmux)
1. [There is No Ghost in My Machine](/revolution)
1. [OSex Change](/osex)
1. [Why Bitters](/bitters)
1. [10 Tools to Gaman and Learn](/tentools)
1. [Arch](/arch)
1. [Dominic Tarr](/dominictarr)
1. [How and Why Use IRC](/irc)

#### Current Tech Offerings

1. [Align Your Webstite](http://align.gwenbell.com)

#### Stack

+ [Arch](/arch): operating system
+ Terminology: terminal emulator
+ [IRC](/irc) & [tmux](/tmux): digital communications
+ dwm: tiling window manager
+ systemd: system/service manager
+ [Node.js](#node) 
+ [Git: distributed version control system](#git)
+ Vim: text editor
+ zsh: shell
+ Gitboria, [Gitlab](http://gitlab.com/u/gwenbell): repo management
+ mplayer, alsamixer: music 
+ Gimp, Inkscape: graphic editors
+ Acer Aspire S3: hardware
+ [Digital Ocean](https://www.digitalocean.com/?refcode=14a7846491d4) (rewards link): VPS
+ [NameCheap](http://www.namecheap.com/?aff=56155) (rewards link): domain registrar

#### <a id = "node"></a>  Node

I built this site on Node.js, using Express, Jade and Stylus. I'm using Bouncy to keep it up, along with systemd.

Learning Node was the gateway for me. It put me on the path to becoming a programmer. That and Twilight Imperium.

#### <a id = "git"></a>  Git

I thought Git sounded like a pain in the ass to learn. Indeed, it was. 

But since I locked myself in a basement and learned to use version control, I can't remember a time when I didn't. But it's a little like when I owned a yoga studio and Patrick would say to me, "you don't understand the pain of being overweight. You have no idea what it's like to be fat, because you've never been fat." 

He was right. But this time I _do_ remember the pain of FTP and a lack of version control.

With Git

1. My work is cryptographically secure
1. I can always go back in time to a previous hash (which is good, because I tend to over-delete)
1. I can lose everything - my laptop, site hosting, everything, and still have a snapshot of my work
1. I don't save to 'the cloud' which, as we know, is a bunch of bullshit anyway

#### <a id = "harmful"></a> Deemed Harmful

Twxttr: You could try [IRC](/irc) instead. I'm in #gaman and #bitters on Freenode

xphone: I've lived without a mobile device since January 2012

Fxxxbook: Distracting to the point of dangerous; clean since 2011

Gxxgle products: I [quit all Gxxgle products](http://gwenbell.com/write#ditchpoogle) in 2012 after going to Kansas City to watch the rollout of Fiber. [Gxxgle's algorithms discriminate against women](http://www.theguardian.com/commentisfree/2013/oct/22/google-autocomplete-un-women-ad-discrimination-algorithms). They can ['suggest'](http://www.engadget.com/2012/03/26/japanese-court-orders-google-to-halt-instant-search-for-suggesti/) you out of a job.

#### Why I do this work

While I advocate digital sabbaticals -- taking time away from the machine -- I'm in tech for the long haul. Each time I see a way to upgrade my stack and therefore work, I make it. 

Each time I see a way to pass that information back to my readers, whether in free articles or paid products, I do it. My work is proof you can make the switch to open/free software _and_ make a living doing what you love. If you're looking for technomemoir, it's [here](/write).
