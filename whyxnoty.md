### Why x Tech, Not y Tech?

It's the number one question I'm getting right now: "Why x technology and Not y technology?" You're asing why [Arch](/arch), not [OSX](/osex)? Why [IRC](/irc), not [Twxttxr](/tech)? Why hard things, not easy ones?

When I hear these questions I interpret them: "Why (Superior Technology) x and not (Inferior Technology) y?"

Let's take a brief trip to my high school peer mediation class. Can you see the beanbag chairs? Can you see my Etnies? Do you see Bob Barden coming into the room all fuzzy beard, waving his pencil around saying today's the day. We're going to take the Meyer's Briggs test.

I was 17. I scored off the charts on two of the letters: N and J. The N is for Intuitive. The J is for Judging. That's a brutal combination: I intuit what's hot, what's now, what's relevant. And I'm usually right. And then I judge you when you've fallen behind.

Because I know that about myself, and have for years (thank you, Bob Barden), I've tried to slow the judge (to give you a chance) and balance it out with some fact-based research (so the intuitive faeries don't run the whole show).

#### Because You've Fallen Behind

Judging. Intuitive. 

And I'm usually right. 

So if what I'm about to say stings, it's because you know it's true.

When you ask me why you should switch to a superior technology when you're "fine" with your inferior technology, what you're saying is:

"I am too lazy to do something hard, and I'm too proud to admit that I'm using this paperclipped/bubblegummed together PDF-to-Dropbox-to-iChat-to-Skype-to-Fxxbook-to-Gxxgle 'workflow' TO LOOK BUSY."

#### And You Want to be a Wizard

What's the solution? Start deleting. Anything that's bullshit and you know it, delete it. 

Then, start learning some hard things. One, [IRC](/irc). I'm in there all the time and I'm not going to 'pop on Skype' just because you're too lazy to learn how to use IRC. Once you learn IRC, learn [tmux](/tmux). And what you're going to discover is you'll want to be on [Arch](/arch) because all the wizards are.

You want to be a wizard. Not a nearly balding thirtysomething.

You want to be a wizard. And one more Fxxxbook [Like](/like) will not a wizard make.

So that is why x and not y.

[Gaman](/gaman). 

Once you figure out IRC (I know you can), I'm in #gaman. 

And if I'm not, I'm tmuxin', so I'll get your message.
