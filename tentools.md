###Gaman: Ten Tech Tools You'll Hate Learning But You Gotta Do it Anyway

In Japanese, the word for celebrating unfun work is _gaman_. You have to do it, you have to push through, even though it's not fun at all. You don't want to do it and you just have to..._gaman_. 

**1. Git**

Top benefit of learning Git: every commit is cryptographically hashed to the previous hash. And you'll not only know what that means, you'll be able to do it.

Remember when you had those candy necklaces as a kid and you ate one off and the necklace stayed together? That is not quite the right metaphor, and to tell you the truth, a better metaphor is one I used when I taught Git at the Flatiron School in Manhattan in June. And that is, like Voldemort, your work is more secure when it's distributed.

The thing about Git is this. You push your work to remotes, of which GitHub may be one but does not need to be, and indeed, should not be, [the only one - Git is not GitHub](/gitnotgithub). Why? Because you know how SPOILER ALERT Voldemort divides his soul and puts it into receptacles and deposits them in more and more obscure places, and you can only destroy him if you reunite all his soul and demolish that? 

You do

	git remote add [name of the remote] [location of the remote]

You're adding your work to another place, making it more resilient.

That is at its most basic what makes Git great. It's why I'm watching [Cypher](https://github.com/dominictarr/cyphernet) so close. An indestructable web is one that is distributed.

Also.

I will tell you this, because of Git, I am a better writer, and a better coder, and possibly a better lover (you will have to ask my many lovers to confirm).

And. AND!

If you call yourself a programmer and don't know Git, you don't know shit.

I do [one on ones](/pairprogram) if you need to get this shit fast.

9/10 on the Gaman Scale. Hard to learn, but not impossible. The reward is worth more than the effort you'll have to put in learning it. Once you learn it you can contribute to the work of other programmers, and they to yours.

**2. Vim**

Top benefit of learning Vim: you don't leave the command line when you need to make an edit to your work.

Another tool that will not make you a better lover, but will make you nerdier. 

Vim I avoided because I thought Sublime Text 2 was just as good. And I'd already upgraded from Scrivener to Sublime Text. 

Then Sublime Text 3 didn't come out and didn't come out, and it cost money anyway, and Vim is free. Also, the guy who works on Vim, he just struck me as a funny guy I'd like to spend time with.

And like Git, the aforementioned hard technology, it is a three letter word created by someone with way more years on the command line than I (will ever?) have. You have to trust (and then verify) these guys know more than you about programming.

Two tools that helped me learn:

1. vim-adventures.com
2. vimtutor which is built in

7/10 on the Gaman Scale. Learning Vim forces you to think about things in a different way than you do now, if you use Sublime Text or another text editor.

**3. The Terminal aka The Command Line aka The CLI**

Top benefit of learning the command line: gateway drug to programming.

Had I not gotten onto the command line, I would not be here telling you any of this. I'd be using Sublime Text to write poetry or something.

Getting the basic commands down - learning how to 

	cd 

around and

	rm -rf
 
to get past gui-dependence, to use the machine how it's meant to be used, as files and directories instead of cutesy smiley faces or whatever chipper icons A**le now uses for what it calls an operating system.

8/10 on the Gaman Scale. Not that hard to get down the basics, but a lifetime of learning codes to make your life even better.

**4. Arch Linux**

Top benefit of learning Arch: freedom in an operating system! Can't overstate this one.

I just wrote about the [benefits of Arch](/arch), and I'll recap for you: it's hard, you might brick your machine. And it's the best operating system I've ever used.

Before this I used Mac OSX, then Ubuntu, then Arch - first with Gnome, and now with Enlightenment. All told, the transition took about a year, including six months on Ubuntu, switching back to Mac for a few months, and then onto Arch. I can't see myself using anything else.

**4b. xmonad**

24 hours ago I installed and started using [xmonad](https://en.wikipedia.org/wiki/Xmonad). I think it's rewiring my brain. It amazes me it's taken this long for me to find it.

Tiling - brace for another incomplete metaphor - remember those games you played as a kid, where you had eight pictures and one blank spot and you had to push them around to make the picture look right. xmonad is similar in that you tile the windows instead of what I've done for years - piling them on top of each other, creating digital hutches, forgetting where I left a window. Maybe you don't hutch. Maybe you complete one task at a time, because you are a unitasking unicorn.

I am not. xmonad changes everything for me. Maybe it will for you, too. You can run it atop Enlightenment, but you gotta get Arch on your box first.

**4c. dwm**

[Dynamic Window Manager](http://dwm.suckless.org/). It has changed my life.

10/10 on the Gaman Scale. Hard. Time consuming. Nobody will understand why you're doing it. Worth it for the freedom. 

**5. Node.js**

Top benefit of learning Node.js: Gets you onto the command line, making commits, learning JavaScript, and surrounds you with dynamic, smart people. 

That's a lot of benefits. 

Node is JavaScript on the server. When I make changes to this site I :wq and then 

	node app

and I'm looking at the beauty that's Express, Jade, Stylus, Markdown and Node. Bitters is a minimalist publishing platform that exists thanks to Node. Learn Git. Then [clone it down](http://github.com/gwenbell/bitters) and find out for yourself.

9/10 on the Gaman Scale. Hard. Time-consuming, and a life changing way to invest your time. If you're already a programmer and don't use Node, you might find it reawakens your love of programming. If you're new to programming, you may find it a good first step into this world. 

**6. Living Phone-Free**

Top benefit of living phone-free: no distractions.

10/10 on the Gaman Scale. Hard habit to break. I believe it's the best possible digital addiction to quit. 

**7. IRC**

Top benefit of learning to use [IRC](/irc): people are frank in IRC. It's okay to admit you don't know, and somebody, in some channel somewhere, will help you.

While you're at it, use [tmux](/tmux) to log the channel. (I formerly suggested Screen, but it's been deemed harmful by the tech community.)

7/10 on the Gaman Scale. Not too hard to learn, but kind of a pain in the ass to blend into your workflow if you're used to Xting and gchatting.

**8. VPS**

Top benefit of learning to use a VPS: peace of mind.

A VPS (virtual private server) is a tiny virtual machine that runs all of the time on a super-fast internet connection. It's virtual, because there are many tiny virtual machines inside of a big rack of superfast computers... somewhere, depending on where you get your VPS from.

Have you ever had access to a timeshare? This is a timeshare on someone's supercomputer.

That way, you don't need a hosting company that tries to do all things - it's the UNIX philosophy - one thing well.

With my VPS I have an always-on Arch Linux, which is how I run Screen, which I mentioned in the IRC section.

If you use [Digital Ocean](https://www.digitalocean.com/?refcode=14a7846491d4), you throw me some referral credit.

6/10 on the Gaman Scale. If you already know Git, FTP is a moot point, and VPS is an obvious next step.

**9. CJDNS**

Top benefit of learning to use CJDNS: security through C.

[CJDNS](https://github.com/cjdelisle/cjdns) is a secure routing protocol that rides on top of IPV6 created by CJD. If you want to learn more about it, visit Derp and Luke's [Hyperboria explainer page](http://hyperboria.net).

8/10 on the Gaman Scale. You can do it, but it will take time and patience. On the other side of that patience is a smart group of people.

**10. SSH**

Top benefit of learning SSH: a cryptographic connection over insecure networks.

You can use SSH to control another machine over an Internet connection without anyone seeing what you're doing. You generate a private key on your machine, and hand a public key to your VPS and voila! Security.

You've seen the word cryptography over and over again in this piece. You're going to see it here one last time before I let you fall asleep and chew on this. Because, when you're doing hard things you gotta rest and let it seep in.

7/10 SSH is a network protocol, in a simliar vein to email - you don't have to understand how it works to use it. 

<div style="font-size: .6em">Updated 24 November 2013</div>
