### Things I Learned From Dominic Tarr

[Dominic Tarr](http://dominictarr.com) left this morning for London. He was here for a week, during which time I had several hours-long conversations with he and [EB](http://evbogue.com). It was a good omen for the birth day of [Align Your Website](http://align.gwenbell.com).

I had the fortune of seeing him off. Before he's too far gone, I want to share some of what he shared with me.

#### Hammocks!

I mentioned today that Oakland is publishing reports about how trash is piling up in the streets. And that one of the worst kinds of trash is beds. In Japan, we had futons. They're more biodegradable than Western beds, which are made out of coils and springs and stuffing. And Americans appear to hate to reuse beds. So I tell Dominic.

Dominic said, "Oh. That's easy. Humans have already figured this out. Hammocks!"

Then he talked about how you just need to find two trees to string up a hammock. 

With that I realized I still have a long way to go on my Stoic journey.

#### Random Bits

These are bits from our conversation as Dominic created [this repo](https://github.com/dominictarr/blog-example) which came of me expressing my frustration with the manual aspect of updating routes for [Bitters](http://bitters.gwenbell.com). I said, is there a simpler way to do this. And he goes, "aww, that's easy." And then proceeded to write the aforementioned blog example repo.

fs.watch (watches repo, updates if it sees a change)
date | c xclip, npm install -g xcp

	date | x  cut
	date | c  outputs copypaste

[hipster](http://github.com/dominictarr/hipster) // dominic's homemade text editor replacement for vim, which he calls hip.

He uses the [Dvorak simplified keyboard layout](https://duckduckgo.com/Dvorak_Simplified_Keyboard). You've never seen anything like it in action. (Unless...you have.)

He brought up [The Bowling Score Kata](http://www.butunclebob.com/ArticleS.UncleBob.TheBowlingGameKata). 

He mentioned [Douglas Engelbart's The Mother of All Demos](https://en.wikipedia.org/wiki/The_Mother_of_All_Demos).

#### Arch

I learned much more than this. And along with EB, helped him get [Arch](/arch) onto his box. Which took about seven hours and ended in high fives. 

It was great to have him in Oakland. I think his Cyphernet work is something to watch. 

And, he'd like to see Bitters replace Tumblr. As would I.
