### ABOUT ME

> INTERVIEWER

> Do you revise?

> GIBSON

> Every day, when I sit down with the manuscript, I start at page one and go through the whole thing, revising freely.

> INTERVIEWER

> You revise the whole manuscript every day?

> GIBSON

> I do, though that might consist of only a few small changes. I've done that since my earliest attempts at short stories. It would be really frustrating for me not to be able to do that. I would feel as though I were flying blind.

Every day, when I sit down to my work, I start at page one and go through the whole thing, revising freely. This morning (24 November 2013) I remembered this quote, but not who said it, nor where I found it. When I duckduckgo'd for it I found another writer I hadn't been in touch with for years. I emailed her. Doing so jumpstarted my memory that this was from The Paris Review, which led me to [this interview with William Gibson](http://www.theparisreview.org/interviews/6089/the-art-of-fiction-no-211-william-gibson). 

Though I write very little fiction, his description of process here parallels my own.

#### Right Now 

+ Preparation for [#gaman](/gaman) and learning Spanish

I'm hosting Gaman because I believe in the power of reviewing the year. It helps me detect patterns, things I still need to work with and reminds me to acknowledge progress made. I enjoy ending the year with others doing the same: looking inward. Bonus: [the project](/gaman) gives my current client base a chance to put their Git skills to work for a month.

#### GB at a Glance

+ Occupation: Technical writer
+ Current location: Abroad
+ Born: 364739520 UNIX time
+ Car Status: None
+ Life Status: Everything I own fits into 1.5 bags
+ Education: UNC-Chapel Hill, University of East Anglia
+ Life Philosophy: Stoic (tranquility trumps all)
+ Started career: Teaching English in Kiryu/Gunma, then opened yoga studio in Yokohama
+ Dream: Build the Gwendolyn Round House [2000](/images/2000-gwendolynroundhouse.jpg) / [2013](/images/2013-gwendolynroundhouse.jpg)
+ Childhood: Before mom's death, [raised on military bases](/expat). After her death, raised by maternal grandparents [Nunu](http://nunubell.com) and Bub
+ Daily Habits: [Appreciations](/appreciations), floss, code, write, edit, walk, write, make bed, clean, look out the window, lengthen spine, practice negative visualization

#### About This Site

Hand-crafted fork of [Bitters](http://bitters.gwenbell.com)

<div style="font-size: .6em">Last updated 24 November 2013</div>
