### WRITING

I've written these pieces over the course of 2013. This is a pared down list; if you'd like access to a piece not listed here, [check my commits](http://gitlab.com/u/gwenbell) or [send a message](mailto:gwen@gwenbell.com) and I'll retrieve it for you.

<a name="top"></a>

#### Technomemoir: Life and Tech

<a href="#opensource">On Open Source: I Don't Exist Without You</a> | <a href="#ditchpoogle">Ditch Google for Good</a> | <a href="#totalbuyin">Total Buy-in</a>

#### Love Letters to Old and/or Dead People

<a href="#engineer">An Engineer Like You</a> | <a href="#tuesday">Tuesday. To the Letter</a> | <a href="#comeout">Come Out Clean</a>

#### That One Time I Got a Divorce

<a href="#divorce">How Else Would Flowers Bloom?</a>

#### That Other Time

<a href="#love">

<hr />

<a name="opensource"></a>
#### On Open Source: I Don't Exist Without You

One of the most frequent questions I get is: _How do I become a programmer like you?_

The answer is you're already in it. Reading this, you're looking at words powered by code I write. You're standing, right now, in an open source digital home called [Bitters](http://bitters.gwenbell.com).

Open source means it's collaborative code. This site is built on Express, which [TJ Holowaychuk](http://tjholowaychuk.com/) first put into the world. And it's powered by Node.js, which Ryan Dhal (currently on sabbatical from what I can tell) first put into the world.

<h4>Mashups: We've seen this before, and yet.</h4>

Open source has been around for years; this work is in remixing code. Those of us in the space spend our time cloning down repos to peek at what's going on under the hood. 

For my part, I like that I get to see the inner workings of some of the minds with which I'm most fascinated. According to my [open source report card](http://osrc.dfm.io/gwenbell), those minds include [Substack](https://github.com/substack), [TJ Holowaychuk](https://github.com/visionmedia) and [Ev Bogue](http://evbogue.com).

We don't exist without each other. In no place is this more obvious than open source.

_It's too hard_ is an easy way to let yourself off the hook. And I'm not trying to get you on the hook, though if you're reading this there's probably a niggling feeling in you: she's right. I gotta go deeper. 

I've become a stronger writer because I've coupled it with programming. I've become a stronger web citizen because I started programming. And I know [I'm not the only one](http://madeleineforbes.co.uk/madeleineforbes.co.uk/code.html).

To get the basics, I'd recommend reading. I recommend Ev's book, [Design Your Website](http://design.evbogue.com/). (Disclosure: I'm the proof-reading editor.) Once you've gotten the basics, you can start cloning down repos. Peer into them. I wrote about how to do this in [Git Commit](http://git.gwenbell.com).

I contribute to the web by creating _for_ it, not just _sucking from it_. I give back. Creating [Bitters](http://bitters.gwenbell.com) was a first step in contributing as a programmer. I built the container I wanted for my work. It's a simple as possible Node.js website. It's the site you're looking at right now.

I get joy when I run across sites using an instance of Bitters. Like last night when I found [Demis Flanagan's](http://www.demisflanagan.com/). Demis didn't ping me to let me know he'd cloned down and created a Bitters site, I just stumbled across it.

It was that moment, it was just. Yeah, Demis!

To know I might have shaved some hours off the time it would have taken him to get a Node site up. To know I might have contributed in some small way to his work, to his life. That is why I bother.

How do we become more self-reliant citizens of the web? By [committing](http://git.gwenbell.com). By contributing. 

We become self-reliant citizens of the web one line of code at a time.

Gwen <br />
23 May 2013 <br />
Brooklyn, New York <br />
<a href="#top">Top</a>

<hr />

<a name="ditchpoogle"></a>
#### Ditch Google for Good

If you never hit delete, you never know what you have. To find out what you have, delete.

If you want to know how dependent you are on something, get locked out of it. It happened as I sipped on a coconut in Mexico. I got locked out of my Google account and realized my digital life was a wobbly, one-legged Google stool. 

That was December 2011. By December 2012 I'd deleted my Google account. This is how I got off and have been able to stay off, Google products.

Ditching things requires you make a decision. A series of decisions, starting with processing your inbox. Process your inbox. Even with all the systems we've created to streamline our inboxes, we still fail to process them completely. To process it one final time, I went through my entire GMail box and made a decision about every message, stretching back to 2007.

Then I

+ moved any sensitive messages to a hard drive
+ had conversations with any people I needed to if there were things unresolved
+ responded, no matter how old the message was, and did not apologize for doing so

I was already using gwen at gwenbell.com. I was redirecting it to Google. I simply killed the redirect and now use Thunderbird full-time.

After processing to actual zero, I took some time away from it. Just because it's now at actual zero doesn't mean you're ready to delete. I took three months to delete my GMail account once I'd completely processed it. Then, I got out of my [filter bubble](http://dontbubble.us) by switching my searches to Duck Duck Go. I now say, "hold on, let me duck duck go that". Then, I removed myself from the Google cloud by migrating anything on Google Drive to my own external hard drive. Then I created a duplicate of that hard drive, so if I lose or damage one, I have a copy.

When I [co-write](/book) something, I use Git rather than the cloud-based Google Docs.

For a browser, I now use surf or [Firefox, Nightly Build](http://nightly.mozilla.org/).

When you leave the ecosystem, leave completely. Avoid sites that talk about Google at length, and stay away from sources you once enjoyed that are Google-centric. The investment to do so is time, and paying closer attention. If you never hit delete, you never know what you have. What's left after you hit delete? Just sit there and wonder.

Our work becomes more resilient through deletion.

<hr />

<a href="#top">Top</a>

<a name="totalbuyin"></a>

#### Total Buy-in

<p class="lead">What if you could let the part of your brain with EARN stamped on it rest for a while? Or forever?</p>

I watched Synecdoche, New York yesterday. Then I walked from Brooklyn to Manhattan, New York as the sun set. I walked a total of 16,557 steps, which according to my iPod Nano watch was 7.83 miles. I saw four condoms in various states of used. I saw two people sleeping on the street. I saw one woman with platinum hair run from one end to the other and back in the time it took me to get to the end of the Williamsburg bridge.

Ev suggested Synecdoche because he knows my love for intense programming. 

My two favorite shows of all time are Six Feet Under, which I watched in its entirety as I learned to program last year. And The Wire, which I almost gave up on because I was bored stiff during the first episode. I stayed with it only because Madeleine Forbes and Ev were all, "give it a chance, man," and now I love it.

The other reason Ev suggested Synecdoche is since the start of the week I've given a lot of thought to genius grants. These things actually exist. They're called MacArthur Fellowships. It's all anonymous, which is also true for [Gittip](http://gittip.com), which I view as distributed, peer-to-peer genius grants. Lots of people supporting each other financially so they can work on and do what they love. Those who have more to give, contribute more. Those who are doing the work and need more support, get more.

In Synecdoche, the main character, Caden Cotard (played by Philip Seymour Hoffman) is given one such genius grant (though in his case it's a lump-sum from higher up, rather than little bits from lots of people - his is vertical, rather than horizontal) about a quarter of the way through the film. He goes from getting by to having so much income he can do whatever he wants with his time and never thinks about earning again. 

He lets the part of his brain stamped EARN take a permanent vacation.

Sounds great! But when you think about it, especially if you've watched The Wire, you know what I mean when I say The Hustle gives you Purpose. It does. The drive to get some product out the door gives you laser-like focus, and without the boundaries of 'you're running out of cashmoney', you've got to get that pushback from _somewhere_.

Caden Cotard is tragic - not semi-tragic, not kinda pathetic melancholy in an Eternal Sunshine kinda way. No, I find him flat-out tragic. This play he's making, funded by the genius grant, goes on for most of eternity, and I don't want to spoil the plot, but there's really no saving graces to this guy. I keep wanting for there to be. But with unlimited funding, it's like he has a permanent case of The Resistance.

If you read my work during 2011 and 2012, you know I got obsessed with The Resistance. After I read _The War of Art_ I started seeing The Resistance everywhere, in everybody, including myself. I couldn't not see it. I was grateful Steven Pressfield had written, finally, the truth about getting things into the world. He covered, in a slim volume, basically every kind of cop out, every excuse I had for not doing the work. Once I got to the end I was all, 'huh. I've done every single one of these things. Now that I know about the Resistance, I'll aim to never have it again!' 

That has almost but not quite worked out. 

Anyway, Caden has The Resistance (which, by the way, is contagious), but there's something more pernicious at play. He spends the duration of the film trying to get buy-in. From anybody. His wife, his second wife, his daughter, the people in the play he's staging. He wants them to say you're an alright kinda guy Caden. You're doing your best. And even when they _do_ say that, he never takes it to heart. He keeps berating himself, wishing he could get _total buy-in_ from _the whole world_. Failing that, he keeps plugging on this play, trying to get _it_ perfect. (Trying to get it perfect is a symptom of the Resistance. You have to hit publish, even if you think it's shit, which you always a little bit will. Think it's shit, that is.)

The only person who can give you total buy-in on your life is you.

There's also no pleasure to this guy's life. He's so caught up in his own _dukkha_ which means dissatisfaction in Buddhist, that he can't see straight. Sure, he fucks but it's always sad. You know, like he's hoping he could perform a little better. He's weepy and self-absorbed. (Contrast that with self-contained.) 

He never merges with his lover. He holds her at arm's length, wanting, you guessed it. 

Total buy-in.

The only person who will give you total buy-in is you. That's why when someone says to me, 'Oh, I could never write like you do. My life's not that interesting,' I take them at their word. 

I see they haven't bought into their own story.

This isn't to say you don't have to drop the story-_line_. My work isn't just in telling masturbatory stories I like to write that you'll like to read. It's also unplugging for two years to listen to my own voice. To discover whether what I'm telling you is true is true for me, or is it just what you (they, the group/collective) want to hear.

Which. Is why I blank the slate. What remains after you blank the slate is as close to true as you're going to get. And you're never going to get to total blank. 

You will never come out clean.

That is what I love about Six Feet Under. None of those characters come out clean, and still, I forgive each one. I forgive them their foibles and 'shortcomings'. 

I forgive the parts of them I thought I couldn't forgive in myself.

They remind me I can let myself off the hook. Caden never lets himself off the hook. He never builds his pleasure palette. He never takes stock of what brings him geniune pleasure.

And I wonder if that's because _he never bottoms out_. I've bottomed out so many times now. I chuckle when I think about them. I don't think my mini-tragedies are actually all that tragic. Yes, of course, hard at the time. Yes, if you're going through hell keep going. But maybe it was during these past two years during a self-chosen exile that showed me: it could always be worse. And it has made pleasure _that much easier_ to touch, each moment.

I didn't always say that. I used to say, "this is the worst thing of all time!" High drama. I'd fling myself on the couch and have a cry for the afternoon.

But when I turn anything on its head and say, "it could always be worse," and "what's the worst that could happen here?" I see the situation differently. That difference, though slim, hasn't just once saved my life. It keeps saving my life.

Whether you think mine is a life worth living means not a whit to me. 

Because the person who has to demonstrate total buy-in is me.

And I do.

<hr />

Further reading: [Switzerland's Proposal to Pay People for Being Alive](http://www.nytimes.com/2013/11/17/magazine/switzerlands-proposal-to-pay-people-for-being-alive.html?smid=pl-share)

<a href="#top">Top</a>

<a name="engineer"></a>

#### An Engineer Like You

I called Bub to remind him of when I was little and I'd beg to go with him to the hospital. 

Bub worked shift work his whole life, and most of the time I knew him growing up, he did the shift work in a hospital. 

We're both engineers, though he did his work with his hands. Bub never used a computer for work, but he did have a man cave he'd go into when he got to the hospital to start his shift, and that was one of the things I liked best about it. That there was this secret place in this ginormous hospital where he, and sometimes I, could go hide out. 

The engineering break room was long and narrow with a bed in the back against the wall. There was a pinup calendar with voluptuous women in tiny outfits in front of motorcycles. The room smelled of tools, of grease and of men, since those were the only things in there most of the time.

I actually hated hospitals is the thing. I didn't want to be in the hospital itself, because it was the same place my mom spent a lot of her five years dying.

Anyway, engineering. Bub's shift. The thing is. When I called him to remind him of how I loved going into work with him, he didn't start reflecting back what I was expecting to hear. He said

Honey, you remember when you wanted to take a shower out there after the hurricane?

No, what do you mean take a shower? At the hospital?

Mmhmm, right. I took you in there in the Renault and you remember that curve at the KOA, a tree came right in through the windshield. Whole tree branch.

OhmygodBub. I could have died!

Sure right you could have. Nearly got your face, I... (he trails) You don't remember that?

Well, huh. Yeah, no. I guess I don't.

Well, we turned back and got a different car. And then I got that generator so you could take showers. Yup, sure did.

<hr /> 

Sometimes I don't get out of him what I went into the call for, but I get something that jump starts another line of thinking. Which of course is ok with me. Bub's in his seventies now, which you'd think would dull him. But the opposite has happened. He remembers a whole set of data that I have altogether forgotten. He has an engineering mind, he sees systems I don't.

Looking at the hospital as an engineer isn't the same as being a patient there. I didn't feel scared when I hung with Bub. Even when very little, when I felt lonely or sad or tired I'd grab a hand, and his hands were usually a little greasy, and always warm. And I knew I could trust him as we navigated the labyrinthine hospital up to the roof. Where we'd check in on the fans. And then down to the generator room, which was still running during the hurricane, hence the shower I was able to take.

The generators can't go down at a hospital. That's the mission critical room. Generators make it so any terrible thing can happen and those patients on respirators, those patients who are in compromised situations, they still get the support they need.

<hr />

As I've become an engineer myself, I experience the joy and challenge of being relied upon. Not relied upon as in if this goes down, people will die. I'm not that kind of engineer. But when I see an ECONNREFUSED I know there are people on the other side of their machines getting something other than they expect. I go into the generator room that is my virtual private server, and I start -ef'ing around, killing and restarting processes.

And, I go into diagnosis mode. If I can't right the ship immediately, I step away from the machine and reapproach again, once I've thought about it.

Becoming an engineer has slowed me down. I think in a different way now. 

I like to imagine I think more like Bub.

<hr />

Before we hung up the call I said

Hey Bub, listen. I don't think I've ever really told you this, but I respect the work you've done. Thank you for showing me how I could become an engineer like you.

Mmhmm, well, alright honey. Call your grandmother, she's over at Et's.

Gwen <br />
9 May 2013 <br />
Brooklyn, New York <br />

<a href="#top">Top</a>

<hr />

<a name="tuesday"></a>

#### Tuesday. To the Letter.

I'm coming into the final stretch for _Terminal_. (_Terminal_ was a limited-time offering to those subscribed to the letter.)

I remember hearing the phrase, "I didn't write that, it wrote me." 

And I remember thinking, every time I heard that, yeah fucking right you didn't write it. If you didn't write it, who wrote it? Because someone had to show up to the page.

Now, I get it. I mean, I _get_ it because I _got_ this one. I got _Terminal_ from the astral plane. I think that sounds weird as fuck. I think it sounds like I was possessed or something. And I guess in a way I have been.

It started with a visit from my mom. She visits about once a decade, but she's visited twice in the time I've been in the city. I watched Ghostbusters right before I moved here. And it sounds absurd to me, but I feel the spirits in the crypts as I walk past on my way to the Japaneast Village. 

I went in there once, and there were dimes and nickels, and pennies, all over those headstones. And many of the headstones were wiped nearly clean, their names and dates of deaths and epitephs...wiped away by the thousands of feet that have walked in and out of that crypt through the years. 

Who is next? This morning I went into the closet, literally, because it's cold in the house right now and I wanted warmth, so I went in there with the sleeping bag that operates as a duvet cover, and I went in there and the faeries, who are always there, were there.

And so was Biggie Smalls. He talked to me about privacy in an open world. He talked to me about stashes. 

It was ripe in there by the time I rolled out. I mean, me and Biggie, plus faeries in the closet? That is a lot of body.

Who is next? Mom. She is up. 

Steve Jobs. Alan Watts. The faeries who are shot all through this. Where they're from, why they arrived, what happens with them, where you can find them.

Tuesday. To the letter. 

<hr />

Gwen <br />
25 May 2013 <br />
Brooklyn, New York <br />
Cinder and Smoke <br />
<a href="#top">Top</a>

<a name="comeout"></a>
#### Come Out Clean

I'm holding the certificate of my mom's death. It's rusty paper clipped together with the paperwork terminating my father's parental rights.

I didn't know my mom's social security number until I found this piece of paper. I don't think I ever even saw this until last night. I was going through paperwork, hand shredding old documents like the paperwork from the Red Cross, when they came after the house burned down. I've saved trauma paperwork I guess. Having it around helped me remember it was real.

Mom died November 30, 1992. My father's parental rights were terminated on the 7th of February 1994 due to item 10: the Defendant has not provided any support for the minor child since her birth. 

Looking at the time between those two dates, I might as well have lived twenty three lives.

Though the termination of his parental rights is final, the certificate of death is deeply final. It's even a slimmer volume than the termination of parental rights - that's three pages. Mom's death certificate is just one. At the bottom, someone named Gailia signed off on it. I guess Gailia had gotten a pay bump because she hand-scratched out Assistant so that just Deputy shows.

<hr />

While I lived in my grandparent's basement teaching myself how to program I watched every episode of Six Feet Under. It was a treat when I was done coding for the day.

I'd tried to watch it years prior, but couldn't handle the intensity. The opening scenes, at the time I found simultaneously overwhelming and undertasteful. The sex scene in the airport closet? I was all, "who would actually do that?" And, that, I found out, was the point Alan Ball was making.

It was Six Feet Under that prompted my decision to move to New York City. I didn't have to travel anymore. Some things got cleared up in me. 

I saw humanity as sort of a giant human cake in the oven. Once in a while you open the oven door, you go in with a knife. You stick it in to check how it's doing, is it cooked all the way through? Every single time you pull out the knife, it doesn't come out clean. The cake never cooks all the way through.

I saw that in myself. I was able to see that there's no amount of striving, no amount of travel, no amount of work, no amount of doing that will make me stick the knife in and have it come out clean.

There is no clean.

<hr />

I called Bub. 

Bub, I found mom's death certificate.

Well, honey, I wasn't aware it'd gone missing.

No, that's not what I mean. I mean I was going through some papers and it was in there somehow. I'd never seen it.

Mmhmm, well alright.

Well, I just learned her social security number.

Well, honey, I don't even think **I** know her social security number.

<hr />

I called Nunu.

We were out in the woods stripping out fish entrails, she said.

That's hardcore, I said. So listen, I just found mom's death certificate. Don't you reckon they have to recycle social security numbers? I just saw hers for the first time.

I suppose they do, don't they? I heard that one time, that they reuse them once the deceased has been gone twenty years.

Nunu, it's been that long now.

I know that is true.

<hr />

The cause of death is asked. There are all sorts of other questions, but under 'Due to or as a consequence of' there's Hodgkins Disease, which they don't know but I do, started as a lump in her neck. And next to that, approximate interval between onset and death: 5 years.

There is no question about how much money was in her bank account. There is no question about how much cash was in her wallet. There is no question about was her inbox at zero. There is no question about was there just this one last thing she wanted to get done real quick before we print this sucker out?

In death, we become one piece of paper, a single entry in the register of deeds. 

Our final selves, booked.

Gwen <br />
7 May 2013 <br />
Bed-Stuy Brooklyn <br />
New York City <br />

<a href="#top">Top</a>

<a name="divorce">How else would flowers bloom?</a>

#### How Else Would Flowers Bloom?

**November 2013 updated: I've published very little on the divorce. This is one of the few pieces. I first published it to the letter in April 2013. I've added it again because if you're going through it, [gaman](/gaman).**

"No way. You got a divorce! But you're so young!"

I laughed. I told her she can ask me anything. I'm ready to talk about it now.

I initiated the divorce while he was in jail. He was in jail, and there was a restraining order out against him, created by the state to protect me.

Boundaries.

Sometimes the state lends a hand with that.

While he was in jail, I told him I was divorcing him and then we didn't talk for weeks. It would have been illegal for him to contact me. It's called a no contact.

I flew to New York. I started writing to the paid letter. I did yoga. I drank. And that's about all I did. Though I sought refuge, I didn't find New York stabilizing. The upshot was I found myself suddenly surrounded by others with their own fierce lives. Photographers, models, heavy smokers who like drum and bass.

And that, yeah. Helped in its own weird way.

That was two years ago this month.

I write this letter to celebrate. It has taken two years to write to you about it, and now I can.

When it happened, I went dark. I killed social profiles to get space and teach the algorithms we were no longer connected. That created the Streisand Effect. When you're as public and enthusiastic about your life as I am mine, well. Of course there was curiosity. Odd thing, hardly anyone contacted me directly about it. When I did reach out to internet friends I knew AFK, I was met with silence. I'm still a little tender around that.

And. I know they have their own things going on. As when mom died, I learned we deal with suffering in each our own way.

The way I deal is I go back out there, tender. Knowing the reason I have a heart is to have it broken open, again and again.

How else would flowers bloom?

Gwen <br />
19 April 2013 <br />
Brooklyn, New York <br />

**Two years prior**

It's Spring in New York City. This morning, a rain hammered the fire escape outside my window.

I have stories to tell you. Believe me, I want to tell you everything. Right now, I can only tell you snapshots. I can only tell elements of the experience; I hope you'll understand.

This isn't reticence to share - things changed rapidly within a 12 hour period in April 2011. Following a digital blank slate (in February) my life was also blank slated. It was unexpected (as most things that change things are) and I'm unsure what's next.

What's certain is this: the life events I've shared with you in the past (mom's death, the house burning down, international moves) - I've just experienced another event of that scale. I am realigning not just the site now, now just the digital presence, but the offline work, too.

//

This is a slow unfolding, an unfurling. I'm writing more than I've ever written. Much of it goes to the letter, some stays in the notebook. Some goes on an index card to be hand-shredded.

As my life unfolds the intensity of living simply has brought me to a place of great clarity with my work. I'm taking fewer clients, but better matches. I'm speaking less - and only at engagements with which I resonate - including Blogher this summer.

I'm working with teams (including Language Dept. here in NYC) whose work I grow alongside my own.

//

Increasingly I am aware of the finitude of all this - this life, this site, this body, yours. I'm aware of the ranunculus blooming during long walks in the city. As the work deepens I invite you along, knowing it's practice, all of it.

Gwen <br />
28 April 2011 <br />
Bread <br />
Spring St, NYC<br />

<a href="#top">Top</a>

