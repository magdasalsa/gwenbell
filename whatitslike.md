I lived in Oakland, California September and October 2013. These are my notes from my first few days there.

***

Here's what I've seen, heard and smelled so far.

This morning I woke up, pulled on my sneaks, went out to the just updated chicken coop, and let out the five chickens who have no names. 
[Luk](http://luke.xxx) says he named them all Dinner.

The first night I got here there was a backyard party. Same backyard as the chickens live in but they were in their coop for the night. 

This party was the kind of party I always want to attend but never happens. Everyone talking LevelDB and Git and Node. [Substack](http://substack.net) played videos of experimental voxel-like dragons eating dragons. [Max Ogden](http://maxogden.com) sat still at several points, people came up to him and asked him how to do JavaScript spells. 

His beard is that long.

There were so many people from #stackvm. The fire was going and it smelled like sweat. I was undershowered and overexcited. That was the pary, as the last day of August clicked over to the first day of September.

Yesterday on the second of September, the chickens laid their first egg. I was already eating the avocado you see in the photo when Max came in with the egg, about the size and weight of a 500 yen coin.

Which reminds me of Japan, which is part of why I am writing to you now. [Vinita](http://vinitasalome.com). We caught each other finally, and I wanted to know about her trip to India this summer. And she has family that visited India from Japan. And she wanted to know about Oakland and programming.

This, Oakland, is a little like landing in Japan. I practiced my Hiragana before I landed in Tokyo, and it wasn't enough but it was something. Still, each day there were moments where things crossed my bow I couldn't understand at all, and I just stood there and sponged or sometimes cried that I couldn't grok.

Still, I kept leaving the house and wandering around and buying all my groceries from the local 7-11 even though it would turn out just a few steps further I would have been at an actual grocery store. But that's how it is when you're in a new land and you have only one or three points of reference.

It's similar here, only it's English but it's Nodelish. And it's Oaklandish. It's chickens cawing bougaunvillas dropping fuschia flowers and worms growing in fermentation experiments. 

That is what Oakland is like.

