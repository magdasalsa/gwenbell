/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 9778);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.locals.pretty = true;
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(require('stylus').middleware(__dirname, '/node_modules/embittered'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(__dirname, 'node_modules/embittered'));
app.use(app.router);

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/whyxnoty', routes.whyxnoty);
app.get('/appreciations', routes.appreciations);
app.get('/tmux', routes.tmux);
app.get('/gaman', routes.gaman);
app.get('/gnome', routes.gnome);
app.get('/irc', routes.irc);
app.get('/ask', routes.ask);
app.get('/expat', routes.expat);
app.get('/path', routes.path);
app.get('/dt', routes.dt);
app.get('/write', routes.write);
app.get('/like', routes.like);
app.get('/tech', routes.tech);
app.get('/book', routes.book);
app.get('/about', routes.about);
app.get('/moretruth', routes.moretruth);
app.get('/whatitslike', routes.whatitslike);
app.get('/pairprogram', routes.pairprogram);
app.get('/arch', routes.arch);
app.get('/tentools', routes.tentools);
app.get('/gitnotgithub', routes.gitnotgithub);
app.get('/whybitters', routes.whybitters);
app.get('/osex', routes.osex);
app.get('/revolution', routes.revolution);
app.get('*', routes.index);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
