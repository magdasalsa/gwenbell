### IRC
#### WHAT IT IS, HOW TO USE IT AND WHY

> Twitter needs to decentralize or it will die. - al3x

I feel responsible. Twitter IPO'd today, and I feel responsible. Not for Twitter. Not for the IPO. But for helping you get off Twitter, because I helped many of you, hundreds and maybe even thousands get on it.

And I see now the importance of helping you at least be aware of, if not help you onto, something open. A protocol, which is what Twitter could have been but [did not](http://al3x.net/2010/09/15/last-thing-about-twitter.html) become.

#### IRC

Trust me, it sounds neck beard nerdy to even be talking about. But now that the IPO has happened, I think it's safe to put Twitter on my [Deemed Harmful list](http://gwenbell.com/tech#harmful). Yes, I'm adding Twitter to the Deemed Harmful list.

Because if we're to have a [revolution](https://en.wikipedia.org/wiki/Internet_Relay_Chat#History), we have to have a way to talk that's non-censorable. 

And Twitter, peeps, as Malcom Gladwell [put it in 2010](http://www.newyorker.com/reporting/2010/10/04/101004fa_fact_gladwell) -- the revolution will not be tweeted.

> Decentralization isn't just a better architecture, it's an architecture that resists censorship and the corrupting influences of capital and marketing. - al3x

So, what's IRC? It's internet relay chat. It's a protocol.

#### HOW TO USE IRC

You get a client. I use [irssi](https://en.wikipedia.org/wiki/Irssi). Then, you choose a username and go into a room.

It takes a little time to get it all set up. But once you do, you'll find there are thousand of active channels, some of which you won't learn about until you've been using IRC for a while (because there's no public, centralized list of IRC channels...and because there are channels you have to be invited into). 

Once you're in a room (I'm in #bitters on Freenode) you simply speak into the room.

It's a little how you might have felt if you were on Twitter in 2007. You, an empty room, just writing onto the wall, having no idea if anyone hears you.

Thing is, if you use a tool like tmux - a [terminal multiplexer](http://www.openbsd.org/cgi-bin/man.cgi?query=tmux&sektion=1) - you can get messages even if you're not in the room when they're sent. A la those little white boards you use in college to let your roomie know you're out of the room for a day or two so she can have some heavy petting sessions in your absence.

#### WHY USE IRC

Ok, so if it's not clear at this point, let me be clear:

1. Twitter is now a publicly traded company. Meaning it answers to share-holders
1. I assume share-holders (of which I am not one) have profit-driven goals
1. I don't care what their goals are; I assume they'll go to any lengths to drive share prices up
1. While I don't begrudge those who make money from this IPO, I'm interested in decentralized ways for people to communicate; Twitter's not it
1. Along the lines of [Git and GitHub](/gitnotgithub) not being the same, Twitter and our ability to communicate are not synonymous
1. IRC has been around since the 80s, and it's not going anywhere, it's not possible to censor it, and it's not IPOable

It's hard to learn (was for me, anyway) but now that I've deemed Twitter harmful, that's where you'll find me. Leave me a message if you come in and I'm not there; I'll check it. I have tmux running.

As for Twitter, we had some good times. Maybe I have the service to thank in part for me being where I am now, a programmer and writer. After Twitter, I'd seen the full range of human expression and possibly human potential. And with a few exceptions, nobody blew me away. One of those exceptions woke up in my bed this morning. And without Twitter, pre-TWTR, could this ever have been? I mean, probably. But actually?

Now. What are we going to call this [end of year](/ask) thing? Because once we decide, I'll make the room on Freenode, and we can go to town.

[Send questions](mailto:gwen@gwenbell.com) or pop into #bitters if there's something in this tutorialish thing that you don't get. It takes some practice.
