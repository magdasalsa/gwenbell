### There's No Ghost in My Machine

I'm pissed for maybe the strangest reason. 

From the time I was old enough to drive I _lived_ in coffee shops. My favorite during high school was The Coffee Scene in Fayetteville aka Fayettenam aka Fayettehell. I loved it because loads of people smoked, everyone shit talked, and nobody had a laptop. There was no laptop camping.

Screens changed cafes, slow-like, over the past decade. You can't be honest with a screen. Dominic asked on Twitter if technology is low empathy. I can't tell you the answer but the THE BACKS OF SCREENS are no empathy. You can't empathize with a screen, no more than you can empathize with a person surrounded by them. At a cafe, in a few foot radius, you're in contact with half a dozen screens, most of them emblazoned with an apple logo.

Now, I rarely visit coffee shops. I hate going into most -- the mood is almost maudlin at many. Yesterday's cafe trip found me in direct glaring line o' fire from the barista. In ye olden times, the baristas were surly, not bitter. They're bitter now, I think, because they see Same Screen Different Day. Nobody talks, and indeed, even in a cafe in SF this past week, nobody screens anything unique. I used to see _dynamic_ shit happening on laptops in the eary days of laptop camping. No longer.

What if the computer revolution hasn't happened yet? That's another piece, but coffee shops were places where you could go on tangents and circle back around and nobody had a glowing red dot telling them they have a MORE VITAL IMPORTANT SPAM MESSAGE IN YOUR INBOX RIGHT NOW YOU HAVE TO GET BACK TO.

Speaking of vital messages. 

My grandfather had a heart attack this weekend. Bub. I wasn't there with him and my grandother, Nunu, for the attack. I got a post-op email last night that I read aloud, not in a coffee shop, but in my home, which I've this month arrived at, cleaned, then stocked with enough Bustelo to be more coffee shop than any cafe I've been in in years. If you come over here I'll be honest with you.

I read the email aloud, then I cried, and it was the most beautiful writing I've read in years.

Of course, because it's personal. And of course, because it's my blood line. But more than that. Because it was honest.

<hr />

This is all I've ever wanted. I want more honesty in my life, in my coffee shop experiences, in my machine.

To get that, I started by exorcizing him. Him. Good ol' Steve.

Jobs that is.

Maybe the mood in cafes is maudlin because people are in a protracted state of mourning? I don't know, but when you look over my shoulder at a coffee shop, not that you'll find me on a laptop at one anymore, [you'd see](/tentools):

1. One screen at a time. At most three, tiled
2. No tabs. I now use [surf](http://suckless.org)
3. Linux. I ride bare metal
4. One social app: tweetdeck
5. dwm: A tiling windows manager, which has changed my life this month
6. Terminal windows, often with written words on them: terminology
7. A mail client: thunderbird

That's it. When I need to go to a site, I mod p, then do

	surf http://duckduckgo.com

Then make a query.

<hr />

It's all very deliberate. I tighten the screws each day. I want this machine light, tight, compact, ghost-free.

But our conversations? Our humans? Our cafes? From these I want more honesty. I want more messiness. I want fewer screens. 

Because we aren't messy in front of our screens. Someone could be listening in from a remote location if we have our iPhone positioned next to our laptops. Right?

I want more of you. I don't want a more empathic screen. There is no such thing!

I started pissed, but I wrote through to accepting. I accept where you're at. 

For now, I'll stay home, away from coffee shops and the ghosts in your machines. 

But you? You can come by any time. 

I'll poweroff for you.

It's the most I can do.

