### EXPAT

> I would like, instead, to be concerned with the question "What is best?", a question which cuts deeply rather than broadly, a question whose answers tend to move the silt downstream. - Robert M. Pirsig

I've lived abroad most of my adult life. Before ancienthood, I spent my growing up years in a military family base-hopping. I got my first passport before I could walk. (I stuck out my tongue for the photo, which was acceptable. Either because I was a baby or because it was the eighties.) I started kindergarten in Germany.

I consider myself an Expatriette. Everything I own fits into one bag. Sometimes I pack a second for bedding, though I think [Dominic](/dt) has the right idea. I don't do pay-to-recommend. If something improves your life and you'd like to say thanks, [gittip](http://gittip.com/gwenbell) me.

#### How to Travel Well: A Short Guide

1. Expect less than you offer to your seat/travel-mate
1. Carry your own weight; if you can't carry it on your person, leave it behind
1. Speak the language _badly_. Make mistakes in the foreign language
1. Eat well-cooked street food
1. Gratitude crosses all languages -- express it
1. Your new mantra: it could always be worse
1. Pack a full tube of toothpaste; it gives the TSA something to do
1. Whenever possible, walk. Wear good walking shoes; who cares how you look

#### Places I Love

+ [Essaouira, Morocco](https://en.wikipedia.org/wiki/Essaouira) (when you get in the cab from Marrakech, be sure to pronounce the r firmly otherwise you'll end up in Essouaila like an idiot, and you will be hot and mad and not at the beach). I learned to surf in Essaouira. Jimi Hendrix did, too. Or, anyway. He played guitar there. Have the bread, hot off the stove, and olives
+ [Kyoto, Japan](https://en.wikipedia.org/wiki/Kyoto): Skip the temples and shrines. Find the hippie van with the tent in the back
+ [Yokohama, Japan](https://en.wikipedia.org/wiki/Yokohama): I opened my yoga studio and lived in Yokohama for three years. That doesn't make me an expert, but it does make me deeply familiar. If you're visiting, [ping me](mailto:gwen@gwenbell.com) for specific-to-you recs
+ [Dingle Peninsula Ireland](https://en.wikipedia.org/wiki/Dingle_peninsula): Here there be faeries
+ New York City: In particular Dough in Bed Stuy, the magical Schoolhouse in Bushwick, Links in Manhattan
+ **Sevilla, Spain**: If you roll into Spain from Morocco (Tangiers), you'll take a water taxi type thing. The Moroccan crowd is not accustomed to traveling on water. Prepare for puke in the bathroom. Pack appropriate footwear
+ [Sayulita, Mexico](https://en.wikipedia.org/wiki/Sayulita): Ask for Tigre on the beach if you want to learn SUP or how to surf. Ask for Jenn if you want a beautiful hand-made necklace. She'll probably be in Chiapas. If she's in town, she'll be with a curly-headed little girl named Luana Africa. Take tea tree oil with you. Have a Ciao Bella (espresso plus ice cream) at Chocobanana
+ [Nusa Lembongan, Bali](https://en.wikipedia.org/wiki/Nusa_Lembongan): This was the first time I snorkeled. I'm spoiled for life. If you visit, you'll see why
+ Chuckanut Forest, Washington: I've asked to have my remains released to the faeries here, where the water meets the treeline

#### Clothes, Shoes, Bags

+ **Bag** Mission Workshop. [Amazing, amazing bag, get the Arkiv hardware](http://missionworkshop.com/products/advanced_projects/vx-rucksack.php)
+ **Shoes** [Minimus from NB](http://www.rei.com/product/845514/new-balance-mt10v2-minimus-trail-running-shoes-mens). You can probably walk a billion miles in them. I've walked not quite that many, but a year in, they've molded themselves to my feet and walk a charm
+ **Mat** Manduka
+ **Camera** Leica D-Lux 5
+ **Warmth** One black pashmina, one striped scarf, the [Amy Ibex](http://shop.ibex.com/Apparel/Womens-Skirts-Dresses/FT-Amy-Dress) <-- I love this dress. Also have two pairs of Ibex hand warmers I wear tons, thanks [Nunu](http://nunubell.com)
