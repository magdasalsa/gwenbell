### Pair Programming && Pair Publishing

I've gotten so many requests from writers who want to learn to program, and from programmers who want to learn to write, I decided to offer pair programming and pair publishing sessions. I offer these to help you get new tech skills if you're a writer and learn to express yourself as a writer if you're a programmer. 

Out of respect for the privacy of those taking these sessions, and because I think they're tacky at this point, I'm not asking for testimonials or endorsements. That said, if you'd like to talk with someone who has taken a session, [email me](mailto:gwen@gwenbell.com) and I'll put you in touch with someone who can speak from their experience about how it went.

**Programming**

Have you been writing for years but are dissatisfied with the CMSs and blogging platforms? 

Do you think that YOU should own your work rather than a megacompany in some posh Silicon Valley office? 

Do you want a house that is fit for the writing you publish? 

Are you willing to do the work to learn to program but have zero idea where to start? 

Would you like to work with someone who can help you get your head around programming from a writer's perspective?

Pair programming sessions get you onto the command line, making Git commits, using Vim and interacting with programmers (or at the very least lurking in an effective way) in IRC. 

Even if you don't want to become an "actual" programmer. Even if you don't plan to push a ton of code. You'll have the fundamentals to begin to create the container you want to display your writing, _how_ you want. This is such a vital thing. You won't know how important the container has been all along until you build it for yourself.

If you're a writer who wants to become a programmer, in one session we'll cover one of the following

1. How to get the right operating system for you
1. How to write HTML, CSS and Markdown to improve your digital life
1. How to use Git in your daily life to make your work more secure
1. How to use IRC to connect with other programmers
1. How to ask tech questions you've been scared to for fear of sounding like an idgit

I will not judge you for not knowing something. A lot of programmers will.

Now why, you ask, would you want to pay for a programming session from someone with a strong communication background? 

Here's why. 

I am ahead of you. I moved off Apple products in 2012. I'm writing this on the command line, in Markdown using Vim. I will commit it to Git once it's ready for publishing. I will communicate things you want to learn in a non-judgy way. 

A programmer with 10,000 hours experience under his (yes, likely his) belt will have neither the time nor the inclination to teach you. He's too busy cashing in his fat programmery checks.

(Also. If you're in need of a brush up on your programming skills, and you're an unmotivated programmer, this session can supercharge your work.)

**Pair Publishing**

Are you sick of having something to say but a lack of decisiveness around how to say it? 

Do you keep prioritizing cranking code over communicating its importance to others? 

Does hardly anyone understand what the effing eff you're going on about? 

Do you need to get into plain English the programming work you've done over the past +/- howevermany years?

Since 2008 I've earned my living publishing, speaking, consulting and teaching. I've published over a dozen courses and ebooks. I know what turns people into readers into fans into paying participants in the work you do. 

I won't say I can work a miracle, but I _know_ the publishing space.

I can help you, programmery person, become a better writer.

If you're a programmer who wants to become a writer, in one session we'll cover one of the following

1. How to commit to publishing even if you think you're a crap writer
1. How to nut graf (publish writing even talented writers will want to read)
1. How to break technical writing into readable bon bons for non-technical readers
1. How to tell a compelling story about yourself and work
1. How to be seen, heard, and understood for the work you do

***

Are you a publisher who wants to program so the house to which you publish is your own? 

Are you a programmer who is ready to publish and have it land for others?

<button>[Get started](mailto:gwen@gwenbell.com)</button>

### How it Works

1. You send an [email](mailto:gwen@gwenbell.com) subject: I WANT ONE + indicate if you'd like to PairProg, PairPub or 50/50
1. Once I receive payment, I send the Pre-Pair Questionnaire (5 questions to pinpoint the starting point from which we jump)
1. At the decided upon time, we connect on Talky.io (webrtc-based service for Chrome/Firefox, ideally Chromium)
1. For 50 minutes we share screens and do the work together
1. That's it! Your personal publishing or programming pocket rocket is refueled!
1. I'll send a one page message summarizing what we covered
1. Repeat as necessary!



