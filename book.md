### How to Book

Here's how to get a book done and out the door fast. 

It requires some programming skills, a second person, trust and focus.

1. Work with a trusted person or agency member.
1. That agent can _only_ correct spelling, fact check and feed you. I mean actual food.
1. Commit all of it to Git from the get-go.
1. Commit it to a private repo in a sacred place. I use Gitboria, which you can only access through Hyperboria. I make the commits to a private repo, and because it's running over CJDNS (and I have access to the code) I trust it's secure.
1. Start from zero with a blank slate. Everything you've already written, if it's good, will show up again. Anything that gets cut shouldn't have been there to begin with!
1. Have your agent - mine is [Ev Bogue from Ven Portman](http://evbogue.com/producingbooks) - set up the files so you can focus on writing, and not at all on the container for the book itself.
1. Use Vim.
1. Git push origin master after each chapter. The agent then does a pull, and alerts you once they've made edits. Then you do a pull. (Of course, you can also branch and merge; I trust this person enough to work on master) (**Warning** this is a high trust situation here).
1. Leave all the details of the cover up to the other person. Your focus is solely on writing.
1. Drop little breadcrumb hints into the public that you're working on a new book; this'll light a fire under you and give your readers-to-be a heads up that you're about to do a release.
1. [Pair publish](/pairprogram) and learn to book with me!
1. Set a date. Do not let yourself off the hook from that date. Having a partner in the process keeps your feet to the fire.
1. Publish! 

How will you know when I've pushed publish? You'll [ask to be notified](http://eepurl.com/Alaev) is how!

[Gwen](/)
